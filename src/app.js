console.log(window);
var Router = ReactRouterDOM.BrowserRouter;
var Route = ReactRouterDOM.Route;
var Link = ReactRouterDOM.Link;

const rootElement = document.getElementById('app')
class App extends React.Component {
render() {
    return (
      <div>
        <div className="mdl-layout mdl-js-layout mdl-layout--fixed-header">
          <header className="mdl-layout__header">
            <div className="mainBar">
              <div className="mainBar mdl-layout__header-row ">
                <div className="mdl-cell mdl-cell--3-col"><Link to="/">  <span className="links title">HEALTHIEST</span></Link></div>
                <Bar/>
              </div>
              <div className="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
                <Link to="/pregled" className="mdl-tabs__tab is_active bottomLinks">Pregled uporabnikov</Link>
                <Link to="/" className="mdl-tabs__tab bottomLinks">Nov uporabnik</Link>
              </div>
            </div>
          </header>
        </div>
        <Route path="/opis" component={Opis} />
        <Route path="/nacrt" component={Nacrt} />
        <Route path="/pregled" component={Pregled} />
        <Route exact path="/" component={Uporabnik} />
      </div>
  );
  }
}

ReactDOM.render(
    <Router>
      <App />
    </Router>
,
  rootElement
)
