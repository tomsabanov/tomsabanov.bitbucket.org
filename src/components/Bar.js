class Bar extends React.Component {
render() {
    return (
      <div className="mdl-grid secondBar">
        <div className="mdl-cell mdl-cell--2-col"><Link to="/opis" className="links">Opis ideje</Link></div>
        <div className="mdl-cell mdl-cell--4-col"><Link to="/nacrt" className="links">Nacrt informacijske resitve</Link></div>
        <div className="mdl-cell mdl-cell--3-col"><button className="mdl-button mdl-js-button mdl-button--raised mdl-button--accent generateButton" onClick={generirajPodatke}>Generiraj podatke</button></div>
      </div>
  );
  }
}
