class Uporabnik extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      ime: '',
      priimek:'',
      datum:'',
      alert1 : null,
      spinner: null,
      ehrid:'7b661e12-3a98-21ad-c29e-2dc9f5a3d885',
      alert2 : null
    }
  }
  addUser(){
      let {ime, priimek, datum} = this.state;
      if (!ime || !priimek || !datum || ime.trim().length == 0 ||
          priimek.trim().length == 0 || datum.trim().length == 0) {
            this.setState({
              alert1:  <div className="alert alert-danger label label-success fade-in">
                        <strong>Napaka!</strong> Prosim vnesite zahtevane podatke!.
                      </div>
            })
    	}
      else{
        let that = this;
        kreirajEHRzaBolnika(ime,priimek,datum)
        .then(function(msg){
            let obj = {
              msg: msg
            }
            if(msg !=  null){
              that.setState({
                alert1: <div className="alert alert-success label label-success fade-in">
                          <span> Uspešno kreiran EHR  {obj.msg}</span>
                        </div>,
                ehrid:obj.msg,
              })
            }
            else if(msg == null){
              that.setState({
                alert1: <div className="alert alert-danger label label-success fade-in">
                          <strong>Napaka!</strong> Prislo je do napake pri dodajanju uporabnika.
                        </div>
              })
            }
        })
      }
  }

  vnesiMeritve(){
    let {ehrid, visina, teza, sist, dist} = this.state;
    let that = this;

    if (!ehrid || ehrid.trim().length == 0) {
      this.setState({
        alert1:  <div className="alert alert-danger label label-success fade-in">
                  <strong>Napaka!</strong> Prosim vnesite zahtevane podatke!.
                </div>
      })
    }
    else{
      dodajMeritveVitalnihZnakov(ehrid,visina, teza, sist, dist)
        .then(function(res){
          if(res == 'success'){
            that.setState({
              alert2: <div className="alert alert-success label label-success fade-in">
                        <span> Uspešno vneseni podatki</span>
                      </div>,
            })
          }
          else{
            that.setState({
              alert2: <div className="alert alert-danger label label-success fade-in">
                        <strong>Napaka!</strong> Prislo je do napake pri vnasanju podatkov.
                      </div>
            })
          }
        })
    }
  }

  render() {

      return (
        <div>
          <div className="addUser">
            <form className="form">
              <div className="form-group">
                <label for="ime">Ime</label>
                <input  value={this.state.ime} onChange={val => this.setState({ime:val.target.value})} type="text" className="form-control" id="inputIme" placeholder="Filthy"/>
              </div>
              <div className="form-group">
                <label for="priimek">Password</label>
                <input value={this.state.priimek} onChange={val => this.setState({priimek:val.target.value})} type="text" className="form-control" id="inputPriimek" placeholder="Frank"/>
              </div>
              <div className="form-group">
                <label for="datum">Password</label>
                <input value={this.state.datum} onChange={val => this.setState({datum:val.target.value})} type="text" className="form-control" id="inputDatum" placeholder="1992-09-18"/>
              </div>
              <button type="button" className="btn btn-primary" onClick={this.addUser.bind(this)}>Submit</button>
            </form>
            {this.state.alert1}
          </div>
          <div className="vnosMeritev">
              <form className="form">
                <span className="label label-default">EHR ID</span><input value={this.state.ehrid} onChange={val => this.setState({ehrid:val.target.value})}  id="dodajVitalnoEHR" type="text" className="form-control input-mini" placeholder={this.state.ehrid}/>
                <span className="label label-default">Telesna višina</span><div className="input-group"><input value={this.state.visina} onChange={val => this.setState({visina:val.target.value})} id="dodajVitalnoTelesnaVisina" type="text" className="form-control input-mini" placeholder="185"/><span className="input-group-addon">cm</span></div>
                <span className="label label-default">Telesna teža</span><div className="input-group"><input value={this.state.teza} onChange={val => this.setState({teza:val.target.value})} id="dodajVitalnoTelesnaTeza" type="text" className="form-control input-mini" placeholder="80.00"/><span className="input-group-addon">kg</span></div>
                <span className="label label-default">Sistolični krvni tlak</span><div className="input-group"><input value={this.state.sist} onChange={val => this.setState({sist:val.target.value})} id="dodajVitalnoKrvniTlakSistolicni" type="text" className="form-control input-mini" placeholder="118"/><span className="input-group-addon">mm Hg</span></div>
                <span className="label label-default">Diastolični krvni tlak</span><div className="input-group"><input value={this.state.dist} onChange={val => this.setState({dist:val.target.value})} id="dodajVitalnoKrvniTlakDiastolicni" type="text" className="form-control input-mini" placeholder="92"/><span className="input-group-addon">mm Hg</span></div>

                <button type="button" className="btn btn-primary" onClick={this.vnesiMeritve.bind(this)}>Submit</button>
              </form>
            {this.state.alert2}
          </div>
        </div>
    );
    }
}
