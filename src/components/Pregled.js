class Pregled extends React.Component {
constructor(props){
  super(props);
  let that = this;

  this.state = {
    ehrid : null,
    alert: null,
    userData: null,
    users : {},
    options: null,
    adviceSist : null,
    adviceDist : null,
    links : null
  }
}
componentDidMount(){
  let that = this;
    generirajPodatke()
    .then(function(res){
      console.log(res);
      that.setState({
        users:res,
      })
    })
}
generirajPodatke(stPacienta) {
  //mi bomo kar generirali vse tri uporabnike, ki bodo imeli vedno znova iste podatke, le ehrid bo  drugacen
  var users = us;
 if(GEN == false){
   GEN = true;
   return kreirajEHRzaBolnika(users.user1.ime,users.user1.priimek,users.user1.datum)
   .then(function(ehrid){
     return dodajMeritveVitalnihZnakov(ehrid, users.user1.visina,users.user1.teza, users.user1.sist, users.user1.dist)
   })
   .then(function(res){
     return kreirajEHRzaBolnika(users.user2.ime,users.user2.priimek,users.user2.datum)
   })
   .then(function(ehrid){
     return dodajMeritveVitalnihZnakov(ehrid, users.user2.visina,users.user2.teza, users.user2.sist, users.user2.dist)
   })
   .then(function(res){
     return kreirajEHRzaBolnika(users.user3.ime,users.user3.priimek,users.user3.datum)
   })
   .then(function(ehrid){
     return dodajMeritveVitalnihZnakov(ehrid, users.user3.visina,users.user3.teza, users.user3.sist, users.user3.dist)
   })
   .then(function(ehrid){
     return window.users;
   })
 }
 else{
   return window.users;
 }

}
pridobiPodatke(){
  let {ehrid} = this.state;
  if (!ehrid || ehrid.trim().length == 0) {
    this.setState({
      alert:  <div className="alert alert-danger label label-success fade-in">
                <strong>Napaka!</strong> Prosim vnesite zahtevane podatke!.
              </div>
    })
  }
  else{
    let that = this;
    let user = {};
    preberiEHRodBolnika(ehrid)
    .then(function(res){
        if(!res.firstNames){
          that.setState({
            alert:  <div className="alert alert-danger label label-success fade-in">
                      <strong>Napaka!</strong> EHR ID {that.state.ehrid} ne obstaja!.
                    </div>
          })
        }
        else{
          user.ime = res.firstNames;
          user.priimek = res.lastNames;
          user.datum = res.dateOfBirth;
          //dobili nazaj uspesen rezultat
          return preberiMeritveVitalnihZnakov(ehrid);
        }
    })
    .then(function(res){
        that.drawUserChart(res.blood_pressure.sist, res.blood_pressure.dist);
        var adviceSist = "";
        var adviceDist = "";
        if(res.blood_pressure.sist > 120){
          adviceSist = "Vas sistolični krvni tlak je previsok!"
        }
        else if(res.blood_pressure.sist < 80){
          adviceSist = "Vas sistolični krvni tlak je prenizek!"
        }
        else{
          adviceSist = "Vas sistolični krvni tlak je idealen"
        }

        if(res.blood_pressure.dist > 80){
          adviceDist = "Vas diastolični krvni tlak je previsok!"
        }
        else if(res.blood_pressure.sist < 60){
          adviceDist = "Vas diastolični krvni tlak je prenizek!"
        }
        else{
          adviceDist = "Vas diastolični krvni tlak je idealen"
        }

        that.setState({
          userData: <table className="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
                            <thead>
                              <tr>
                                <th className="mdl-data-table__cell--non-numeric">{user.ime} {user.priimek}, rojen {user.datum}</th>
                                <th>Visina</th>
                                <th>Teza</th>
                                <th>Sistolični krvni tlak</th>
                                <th>Diastolični krvni tlak</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td className="mdl-data-table__cell--non-numeric"></td>
                                <td>{res.height.num} {res.height.unit}</td>
                                <td>{res.weight.num} {res.weight.unit}</td>
                                <td>{res.blood_pressure.sist} {res.blood_pressure.unit}</td>
                                <td>{res.blood_pressure.dist} {res.blood_pressure.unit}</td>
                              </tr>
                            </tbody>
                          </table>,
          adviceSist : adviceSist,
          adviceDist : adviceDist
        })
        return getLink();
    })
    .then(function(res){
      that.setState({
        links: <div>
                <h2> Malce vec o krvnem pritisku (pridobljeno iz wikipedije, duck duck go)</h2>
                <p>{res.Abstract}</p>
                <p>Ce zelite izvedeti vec o krvnem pritisku, pojdite na {res.AbstractURL}</p>
              </div>
      })
    })
  }
}
drawChart() {
  var data = google.visualization.arrayToDataTable([
          ["Element", "Density", { role: "style" } ],
          ["Sistolični krvni tlak", 120, "blue"],
          ["Diastolični krvni tlak", 80, "blue"],
        ]);

        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
                         { calc: "stringify",
                           sourceColumn: 1,
                           type: "string",
                           role: "annotation" },
                         2]);

        var options = {
          title: "Idealni krvni pritisk",
          width: 600,
          height: 400,
          bar: {groupWidth: "95%"},
          legend: { position: "none" },
        };
        var chart = new google.visualization.ColumnChart(document.getElementById("chart_div_ideal"));
        chart.draw(view, options);
}
drawUserChart(sist,dist){
  var data = google.visualization.arrayToDataTable([
          ["Element", "Density", { role: "style" } ],
          ["Sistolični krvni tlak", sist, "red"],
          ["Diastolični krvni tlak", dist, "red"],
        ]);

        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
                         { calc: "stringify",
                           sourceColumn: 1,
                           type: "string",
                           role: "annotation" },
                         2]);

        var options = {
          title: "Uporabnikov krvni pritisk",
          width: 600,
          height: 400,
          bar: {groupWidth: "95%"},
          legend: { position: "none" },
        };
        var chart = new google.visualization.ColumnChart(document.getElementById("chart_div_user"));
        chart.draw(view, options);
}


render() {
    let users = [];
    for(var key in this.state.users){
      users.push(this.state.users[key]);
    }
    // Load the Visualization API and the corechart package.
    google.charts.load('current', {'packages':['corechart']});

       // Set a callback to run when the Google Visualization API is loaded.
   google.charts.setOnLoadCallback(this.drawChart);

       // Callback that creates and populates a data table,
       // instantiates the pie chart, passes in the data and
       // draws it.

    return (
      <div className="panel panel-default">
      					<div className="panel-heading">
      						<div className="row">
      							<div className="col-lg-8 col-md-8 col-sm-8"><b>Preberi EHR zapis</b> obstoječega bolnika</div>
      							<div className="col-lg-4 col-md-4 col-sm-4">
                      <select className="form-control input-sm" id="preberiObstojeciEHR" onChange={val => this.setState({ehrid:val.target.value})}>

                        {users.map(function(user) {
                          return <option key={user.ehrid} value={user.ehrid}>{user.ime} {user.priimek}</option>;
                        })}


                      </select>
                    </div>
      						</div>
      					</div>
      					<div className="panel-body">
      						<span className="label label-default">EHR ID</span><input  value={this.state.ehrid} onChange={val => this.setState({ehrid:val.target.value})} id="preberiEHRid" className="form-control input-mini" placeholder="7b661e12-3a98-21ad-c29e-2dc9f5a3d885" type="text"/>
      						<button type="button" className="btn btn-primary btn-xs" onClick={this.pridobiPodatke.bind(this)}>Preberi osnovne podatke o bolniku</button><span id="preberiSporocilo"></span>
      				  	</div>
                  {this.state.alert}
                  {this.state.userData}
                <div id="chart_div_ideal"></div>
                <div id="chart_div_user"></div>
                {this.state.adviceDist}
                <br/>
                {this.state.adviceSist}
                {this.state.links}
      </div>
  );
  }
}
