
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
  return new Promise(function(resolve, reject){
    $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        success: function(res){
            resolve(res.sessionId);
        }
    });
  })
}


/**
 * Kreiraj nov EHR zapis za pacienta in dodaj osnovne demografske podatke.
 * V primeru uspešne akcije izpiši sporočilo s pridobljenim EHR ID, sicer
 * izpiši napako.
 */
window.users = {};

function kreirajEHRzaBolnika(ime, priimek, datumRojstva) {
    return getSessionId()
    .then(function(sessionId){
      datumRojstva +="T00:00:00.000Z";
      return new Promise(function(resolve, reject){
        $.ajaxSetup({
            headers: {"Ehr-Session": sessionId}
        });
        $.ajax({
            url: baseUrl + "/ehr",
            type: 'POST',
            success: function (data) {
                var ehrId = data.ehrId;
                var partyData = {
                    firstNames: ime,
                    lastNames: priimek,
                    dateOfBirth: datumRojstva,
                    partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
                };
                $.ajax({
                    url: baseUrl + "/demographics/party",
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify(partyData),
                    success: function (party) {
                        if (party.action == 'CREATE') {
                           //return success
                          window.users[ehrId] = {
                            ime:ime,
                            priimek:priimek,
                            datumRojstva:datumRojstva,
                            ehrid: ehrId
                          }
                          KREIRAJ = ehrId;

                           resolve(ehrId);
                        }
                    },
                    error: function(err) {
                       // return error
                       resolve(null);
                    }
                });
            }
        });
      })

    })
}



/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID, datum in ura,
 * telesna višina, telesna teža, sistolični in diastolični krvni tlak,
 * nasičenost krvi s kisikom in merilec).
 */
function dodajMeritveVitalnihZnakov(ehrId, telesnaVisina, telesnaTeza, sistolicniKrvniTlak, diastolicniKrvniTlak) {
  return getSessionId()
  .then(function(sessionId){
    return new Promise(function(resolve,reject){
      $.ajaxSetup({
          headers: {"Ehr-Session": sessionId}
      });
      var podatki = {
        // Struktura predloge je na voljo na naslednjem spletnem naslovu:
        // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
          "ctx/language": "en",
          "ctx/territory": "SI",
          "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
          "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
          "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
          "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
      };
      var parametriZahteve = {
          ehrId: ehrId,
          templateId: 'Vital Signs',
          format: 'FLAT',
      };
      $.ajax({
          url: baseUrl + "/composition?" + $.param(parametriZahteve),
          type: 'POST',
          contentType: 'application/json',
          data: JSON.stringify(podatki),
          success: function (res) {
              window.users[ehrId].visina = telesnaVisina;
              window.users[ehrId].teza = telesnaTeza;
              window.users[ehrId].sist = sistolicniKrvniTlak;
              window.users[ehrId].dist = diastolicniKrvniTlak;

              resolve('success');
          },
          error: function(err) {
              resolve('error');
          }
      });
    })
  })
}

/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function preberiEHRodBolnika(ehrId) {
	return getSessionId()
  .then(function(sessionId){
    return new Promise(function(resolve, reject){
      $.ajaxSetup({
          headers: {"Ehr-Session": sessionId}
      });
      $.ajax({
        url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
        type: 'GET',
        headers: {"Ehr-Session": sessionId},
          success: function (data) {
          var party = data.party;
          resolve(party);
          /*$("#preberiSporocilo").html("<span class='obvestilo label " +
            "label-success fade-in'>Bolnik '" + party.firstNames + " " +
            party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
            "'.</span>");*/
        },
        error: function(err) {
          resolve('error');
          /*$("#preberiSporocilo").html("<span class='obvestilo label " +
            "label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");*/
        }
      });
    })
  })
}

/**
 * Pridobivanje vseh zgodovinskih podatkov meritev izbranih vitalnih znakov
 * (telesna temperatura, filtriranje telesne temperature in telesna teža).
 * Filtriranje telesne temperature je izvedena z AQL poizvedbo, ki se uporablja
 * za napredno iskanje po zdravstvenih podatkih.
 */
function preberiMeritveVitalnihZnakov(ehrId) {
  var data = {
    weight: {
      num : null,
      unit: null
    },
    height: {
      num : null,
      unit: null
    },
    blood_pressure:{
      dist : null,
      sist : null,
      unit : null
    }
  }
  var sessionId = 0;
	return getSessionId()
  .then(function(session){
    sessionId = session;
    return new Promise(function(resolve,reject){
  					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
  					    type: 'GET',
  					    headers: {"Ehr-Session": sessionId},
  					    success: function (res) {
  					    	if (res.length > 0) {
                      data.weight.num = res[0].weight;
                      data.weight.unit = res[0].unit;
  					    	}
                  resolve();
  					    },
                error: function(res){
                  resolve();
                }
  					});
		});
  })
  .then(function(res){
    return new Promise(function(resolve,reject){
  					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "height",
  					    type: 'GET',
  					    headers: {"Ehr-Session": sessionId},
  					    success: function (res) {
  					    	if (res.length > 0) {
                    data.height.num = res[0].height;
                    data.height.unit = res[0].unit;
  					    	}
                  resolve();
  					    },
                error: function(res){
                  resolve();
                }
  					});
		});
  })
  .then(function(res){
    return new Promise(function(resolve,reject){
            $.ajax({
                url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
                type: 'GET',
                headers: {"Ehr-Session": sessionId},
                success: function (res) {
                  if (res.length > 0) {
                    data.blood_pressure.dist = res[0].diastolic;
                    data.blood_pressure.sist = res[0].systolic;
                    data.blood_pressure.unit = res[0].unit;
                  }
                  resolve(data);
                },
                error: function(res){
                  resolve(data);
                }
            });
    });
  })
}

function getLink(){
  return new Promise(function(resolve,reject){
          $.ajax({
              url: 'http://api.duckduckgo.com/?q=bloodpressure&format=json',
              type: 'GET',
              dataType: "jsonp",
              success: function (res) {
                console.log(res);
                resolve(res);
              },
              error: function(res){
                resolve();
              }
          });
  });
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 var us = {
   user1:{
     ime:'Filthy',
     priimek:'Frank',
     datum:'1992-09-18',
     visina:178,
     teza:90,
     sist: 90,
     dist: 60,
     ehrid:null

   },
   user2:{
     ime:'Pink',
     priimek:'Guy',
     datum:'2004-09-18',
     visina:160,
     teza:70,
     sist:180,
     dist:90,
     ehrid:null
   },
   user3:{
     ime:'Chin',
     priimek:'Chin',
     datum:'1001-01-01',
     visina:210,
     teza:100,
     sist:120,
     dist:80,
     ehrid:null
   }
 }
 var GEN = false;
function generirajPodatke(stPacienta) {
  //mi bomo kar generirali vse tri uporabnike, ki bodo imeli vedno znova iste podatke, le ehrid bo  drugacen
  var users = us;
 if(GEN == false){
   GEN = true;
   return kreirajEHRzaBolnika(users.user1.ime,users.user1.priimek,users.user1.datum)
   .then(function(ehrid){
     return dodajMeritveVitalnihZnakov(ehrid, users.user1.visina,users.user1.teza, users.user1.sist, users.user1.dist)
   })
   .then(function(res){
     return kreirajEHRzaBolnika(users.user2.ime,users.user2.priimek,users.user2.datum)
   })
   .then(function(ehrid){
     return dodajMeritveVitalnihZnakov(ehrid, users.user2.visina,users.user2.teza, users.user2.sist, users.user2.dist)
   })
   .then(function(res){
     return kreirajEHRzaBolnika(users.user3.ime,users.user3.priimek,users.user3.datum)
   })
   .then(function(ehrid){
     return dodajMeritveVitalnihZnakov(ehrid, users.user3.visina,users.user3.teza, users.user3.sist, users.user3.dist)
   })
   .then(function(ehrid){
     return window.users;
   })
 }
 else{
   return window.users;
 }

}
window.generirajPodatke = generirajPodatke;


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
